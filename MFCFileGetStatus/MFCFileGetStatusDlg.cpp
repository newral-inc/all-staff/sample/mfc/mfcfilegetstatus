
// MFCFileGetStatusDlg.cpp : 実装ファイル
//

#include "stdafx.h"
#include "MFCFileGetStatus.h"
#include "MFCFileGetStatusDlg.h"
#include "afxdialogex.h"
#include <atlpath.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMFCFileGetStatusDlg ダイアログ



CMFCFileGetStatusDlg::CMFCFileGetStatusDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMFCFileGetStatusDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFCFileGetStatusDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CMFCFileGetStatusDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
END_MESSAGE_MAP()


// CMFCFileGetStatusDlg メッセージ ハンドラー

BOOL CMFCFileGetStatusDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// このダイアログのアイコンを設定します。アプリケーションのメイン ウィンドウがダイアログでない場合、
	//  Framework は、この設定を自動的に行います。
	SetIcon(m_hIcon, TRUE);			// 大きいアイコンの設定
	SetIcon(m_hIcon, FALSE);		// 小さいアイコンの設定

	// カレントディレクトリの"ReadMe.txt"ファイルの情報を取得します。
	TCHAR path[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, path);
	CPath filePath(path);
	filePath.Append(_T("ReadMe.txt"));
	
	// ファイルの情報を取得します。
	CFileStatus fileStatus;
	if (CFile::GetStatus(filePath, fileStatus))
	{
		// ファイルパス
		CString msg;
		msg.Format(_T("ファイルパス:%s\n"), fileStatus.m_szFullName);
		OutputDebugString(msg);

		// ファイルサイズ
		msg.Format(_T("ファイルサイズ:%d\n"), fileStatus.m_size);
		OutputDebugString(msg);

		// ファイル作成日
		msg = fileStatus.m_ctime.Format(_T("ファイル作成日時:%Y/%m/%d %H:%M:%S\n"));
		OutputDebugString(msg);

		// ファイル最終更新日時
		msg = fileStatus.m_mtime.Format(_T("ファイル最終更新日時:%Y/%m%d %H:%M:%S\n"));
		OutputDebugString(msg);

		// ファイルアクセス日時
		msg = fileStatus.m_atime.Format(_T("ファイルアクセス日時:%Y/%m%d %H:%M:%S\n"));
		OutputDebugString(msg);

		// 属性:読み取り専用
		if (fileStatus.m_attribute & CFile::readOnly)
		{
			OutputDebugString(_T("読み取り専用\n"));
		}

		// 属性:隠しファイル
		if (fileStatus.m_attribute & CFile::hidden)
		{
			OutputDebugString(_T("隠しファイル\n"));
		}

		// 属性:システムファイル
		if (fileStatus.m_attribute & CFile::system)
		{
			OutputDebugString(_T("システムファイル\n"));
		}

		// 属性:ボリューム
		if (fileStatus.m_attribute & CFile::volume)
		{
			OutputDebugString(_T("ボリューム\n"));
		}

		// 属性:ディレクトリ
		if (fileStatus.m_attribute & CFile::directory)
		{
			OutputDebugString(_T("ディレクトリ\n"));
		}

		// 属性:アーカイブ
		if (fileStatus.m_attribute & CFile::archive)
		{
			OutputDebugString(_T("アーカイブ\n"));
		}
	}

	// ファイルが存在しない場合
	else
	{
		OutputDebugString(_T("ファイルが存在しません"));
	}

	return TRUE;  // フォーカスをコントロールに設定した場合を除き、TRUE を返します。
}

// ダイアログに最小化ボタンを追加する場合、アイコンを描画するための
//  下のコードが必要です。ドキュメント/ビュー モデルを使う MFC アプリケーションの場合、
//  これは、Framework によって自動的に設定されます。

void CMFCFileGetStatusDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 描画のデバイス コンテキスト

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// クライアントの四角形領域内の中央
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// アイコンの描画
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// ユーザーが最小化したウィンドウをドラッグしているときに表示するカーソルを取得するために、
//  システムがこの関数を呼び出します。
HCURSOR CMFCFileGetStatusDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

