
// MFCFileGetStatus.h : PROJECT_NAME アプリケーションのメイン ヘッダー ファイルです。
//

#pragma once

#ifndef __AFXWIN_H__
	#error "PCH に対してこのファイルをインクルードする前に 'stdafx.h' をインクルードしてください"
#endif

#include "resource.h"		// メイン シンボル


// CMFCFileGetStatusApp:
// このクラスの実装については、MFCFileGetStatus.cpp を参照してください。
//

class CMFCFileGetStatusApp : public CWinApp
{
public:
	CMFCFileGetStatusApp();

// オーバーライド
public:
	virtual BOOL InitInstance();

// 実装

	DECLARE_MESSAGE_MAP()
};

extern CMFCFileGetStatusApp theApp;